using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class PhysicsUtils
    {
        public static Vector3 CorrectVelocityWithSlope(Vector2 input, float speed, Transform transform )
        {
            Vector3 dir = new Vector3(input.x, 0, 0);
            Vector3 vel = dir * speed;
            vel = PhysicsUtils.DescendingSlope(vel, transform);

            if (dir != Vector3.zero)
            {
                transform.forward = dir;
            }

            return vel;
        }
        public static Vector3 DescendingSlope(Vector3 velocity, Transform transform)
        {
            var ray = new Ray(transform.position, Vector3.down);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, 0.2f))
            {
                var slopeRot = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);

                var adjustVel = slopeRot * velocity;

                if (adjustVel.y < 0)
                    return adjustVel;
            }

            return velocity;
        }

        public static Vector3 JumpHeight(Vector3 playerVelocity, float jumpHeight, float gravityValue)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);

            return playerVelocity;
        }
    }
}

