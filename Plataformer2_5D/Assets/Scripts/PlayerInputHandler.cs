using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerInputHandler : MonoBehaviour
{

    public Vector2 movementInput { get; private set; }
    public bool jumpInput { get; private set; }
    public bool jumpStop { get; private set; }
    [SerializeField]
    private float holdTime = 0.2f;
    private float jumpInputStartTime;

    private void Update()
    {
        CheckJumpHoldTime();
    }

    public void OnMoveInput(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }

    public void OnJumpInput(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            //button down
            jumpInput = true;
            jumpStop = false;
            jumpInputStartTime = Time.time;
        }

        if(context.performed)
        {
            //holding
        }

        if(context.canceled)
        {
            //released
            jumpStop = true;
        }
    }

    public void UsedJumpInput() => jumpInput = false;

    private void CheckJumpHoldTime()
    {
        if(Time.time >= jumpInputStartTime + holdTime)
        {
            jumpInput = false;
        }
    }

    

}
