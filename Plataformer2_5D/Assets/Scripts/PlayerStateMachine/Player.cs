using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    public StateMachine stateMachine { get; private set; }
    public Animator anim { get; private set; }
    public PlayerInputHandler inputHandler { get; private set; }
    public CharacterController controller { get; private set; }
    [SerializeField]
    private PlayerData playerData;
    public Vector3 playerVel;
    [SerializeField]
    private Transform groundChcker;
    [SerializeField]
    private Transform wallChckerStart;
    [SerializeField]
    private Transform wallChckerFinish;
    [SerializeField]
    private LayerMask WhatIsGround;

    public bool grounded;
    public bool onWall;
    public bool facingRight = true;

    #region States
    public PlayerIdleState idleState { get; private set; }
    public PlayerMoveState moveState { get; private set; }
    public PlayerJumpState jumpState { get; private set; }
    public PlayerInAirState airState { get; private set; }
    public PlayerLandState landState { get; private set; }
    public PlayerWallSlideState wallSlideState { get; private set; }
    public PlayerWallJumpState wallJumpState { get; private set; }
    #endregion

    private void Awake()
    {
        stateMachine = new StateMachine();
        idleState = new PlayerIdleState(this, stateMachine, playerData, "idle");
        moveState = new PlayerMoveState(this, stateMachine, playerData, "move");
        jumpState = new PlayerJumpState(this, stateMachine, playerData, "inAir");
        airState = new PlayerInAirState(this, stateMachine, playerData, "inAir");
        landState = new PlayerLandState(this, stateMachine, playerData, "land");
        wallSlideState = new PlayerWallSlideState(this, stateMachine, playerData, "wallSlide");
        wallJumpState = new PlayerWallJumpState(this, stateMachine, playerData, "wallJump");


    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        inputHandler = GetComponent<PlayerInputHandler>();
        controller = GetComponent<CharacterController>();

        stateMachine.Initialize(idleState);
    }

    private void Update()
    {
        checkFaceRight();
        stateMachine.currentState.LogicUpdate();
    }

    private void FixedUpdate()
    {
        grounded = IsGrounded();
        onWall = CheckIfWall();
        stateMachine.currentState.PhysicsUpdate();
    }

    //----------------------
    public void AnimFinishTrigger() => stateMachine.currentState.SetAnimFinish();

    public bool IsGrounded()
    {

        Collider[] hits = Physics.OverlapSphere(groundChcker.position, 0.25f, WhatIsGround);

        if (hits.Length != 0)
            return true;

        return false;
    }

    public bool CheckIfWall()
    {
        return Physics.Linecast(wallChckerStart.position, wallChckerFinish.position);
    }

    private void checkFaceRight()
    {
        if (inputHandler.movementInput.x < 0 && facingRight)
            facingRight = false;
        else if (!facingRight && inputHandler.movementInput.x > 0)
            facingRight = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(groundChcker.position, 0.25f);

        Gizmos.DrawLine(wallChckerStart.position, wallChckerFinish.position  );
    }
}
