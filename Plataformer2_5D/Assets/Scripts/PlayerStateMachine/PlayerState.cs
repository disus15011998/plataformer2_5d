using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState 
{
    protected Player player;
    protected StateMachine stateMachine;
    protected PlayerData playerData;
    protected float startTime;
    protected bool isAnimFinish;

    private string animBoolName;

    public PlayerState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName)
    {
        this.player = player;
        this.stateMachine = stateMachine;
        this.playerData = playerData;
        this.animBoolName = animBoolName;
    }

    public virtual void EnterState() {
        DoChecks();
        player.anim.SetBool(animBoolName, true);
        startTime = Time.time;
        isAnimFinish = false;
    }

    public virtual void ExitState() {
        player.anim.SetBool(animBoolName, false);
    }

    public virtual void LogicUpdate() { }

    public virtual void PhysicsUpdate() { }

    public virtual void DoChecks() { }

    public virtual void SetAnimFinish() => isAnimFinish = true;

    
}
