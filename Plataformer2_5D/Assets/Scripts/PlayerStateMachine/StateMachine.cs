using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine 
{
    public PlayerState currentState { get; private set; } // Estado actual del sistema


    //M�todo para inicializar la maquina de estados
    public void Initialize(PlayerState initialState)
    {
        currentState = initialState;
        currentState.EnterState();
    }

    //M�todo para realizar la trancici�n del estado actual a uno nuevo
    public void ChangeState(PlayerState newState)
    {
        currentState.ExitState();
        currentState = newState;
        currentState.EnterState();
    }
}
