using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTouchingWallState : PlayerState
{
    public PlayerTouchingWallState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void EnterState()
    {
        base.EnterState();
        player.playerVel = Vector3.zero;
    }

    public override void ExitState()
    {
        base.ExitState(); 
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if(player.grounded)
        {
            stateMachine.ChangeState(player.idleState);
        }
        else if(!player.onWall && player.playerVel.y <= 0)
        {
            stateMachine.ChangeState(player.airState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void SetAnimFinish()
    {
        base.SetAnimFinish();
    }
}
