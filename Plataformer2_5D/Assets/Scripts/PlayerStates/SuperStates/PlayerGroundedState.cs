using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundedState : PlayerState
{
    protected Vector2 inputMov;
    protected bool jumpInput;

    public PlayerGroundedState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void EnterState()
    {
        base.EnterState();
        player.playerVel = Vector3.zero;
        player.jumpState.ResetAmountOfJumps();
    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        inputMov = player.inputHandler.movementInput;
        jumpInput = player.inputHandler.jumpInput;

        if (!player.grounded)
        {
            player.airState.StartCoyoteTime();
            stateMachine.ChangeState(player.airState);
        }
        else if (jumpInput && player.jumpState.CanJump())
        {
            player.inputHandler.UsedJumpInput();
            stateMachine.ChangeState(player.jumpState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
