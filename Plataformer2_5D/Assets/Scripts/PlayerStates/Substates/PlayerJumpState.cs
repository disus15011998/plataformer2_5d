using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpState : PlayerAbilityState
{
    private int amountOfJumpsLeft;

    public PlayerJumpState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
        amountOfJumpsLeft = playerData.numOfJumps;
    }

    public override void EnterState()
    {
        base.EnterState();
        player.airState.SetIsJumping();
        RestAmountOfJumps();
        player.playerVel.y = 0;
        player.playerVel = Utils.PhysicsUtils.JumpHeight(player.playerVel, playerData.jumpHeight, playerData.gravity);
        isAbilityDone = true;
        stateMachine.ChangeState(player.airState);
    }

    public bool CanJump() => amountOfJumpsLeft > 0 ? true : false;

    public void ResetAmountOfJumps() => amountOfJumpsLeft = playerData.numOfJumps;

    public void RestAmountOfJumps() => amountOfJumpsLeft--;
}
