using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class PlayerMoveState : PlayerGroundedState
{
    Vector3 playerVelocity;
    public PlayerMoveState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
        
    }

    public override void EnterState()
    {
        base.EnterState();
    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(player.onWall)
            stateMachine.ChangeState(player.idleState);

        Vector3 velocity = PhysicsUtils.CorrectVelocityWithSlope(inputMov, playerData.speed, player.transform);
        player.controller.Move(velocity * Time.deltaTime);
        player.anim.SetFloat("speed", Mathf.Abs(inputMov.x));

        
        if (inputMov.x == 0)
        {
            stateMachine.ChangeState(player.idleState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
