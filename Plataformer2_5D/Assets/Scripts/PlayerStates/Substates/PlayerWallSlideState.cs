using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWallSlideState : PlayerTouchingWallState
{
    public PlayerWallSlideState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void ExitState()
    {
        base.ExitState();
        player.transform.RotateAround(player.transform.position, Vector3.up, 180);
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        Vector3 velocity = Utils.PhysicsUtils.CorrectVelocityWithSlope(player.inputHandler.movementInput, playerData.speedInAir, player.transform);
        player.controller.Move(velocity * Time.deltaTime);

        player.playerVel.y = -playerData.wallSlideVel;
        player.controller.Move(player.playerVel * Time.deltaTime);

        if (player.inputHandler.jumpInput)
        {

            stateMachine.ChangeState(player.wallJumpState);
        }
    }
}
