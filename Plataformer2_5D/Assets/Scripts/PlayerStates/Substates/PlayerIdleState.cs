using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : PlayerGroundedState
{
    bool enterFaceState;
    public PlayerIdleState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
       
    }

    public override void EnterState()
    {
        base.EnterState();
        enterFaceState = player.facingRight;
    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(player.onWall)
            Flip();

        if (inputMov.x != 0 && !player.onWall)
        {
            stateMachine.ChangeState(player.moveState);
        }

    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    void Flip()
    {
        if(player.facingRight != enterFaceState)
            player.transform.RotateAround(player.transform.position, Vector3.up, 180);
    }
}
