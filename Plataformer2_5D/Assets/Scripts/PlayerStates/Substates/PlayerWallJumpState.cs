using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWallJumpState : PlayerTouchingWallState
{
    public PlayerWallJumpState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void EnterState()
    {
        base.EnterState();

        player.airState.SetIsJumping();
        player.playerVel = Vector3.zero;
        player.playerVel = Utils.PhysicsUtils.JumpHeight((player.transform.forward * playerData.wallJumpForce), 1, playerData.gravity);
        


    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        player.playerVel.y += playerData.gravity * Time.deltaTime;
        player.controller.Move(player.playerVel * Time.deltaTime);

        if (player.onWall && player.playerVel.y <= 0)
        {
            stateMachine.ChangeState(player.wallSlideState);
        }

    }
}
