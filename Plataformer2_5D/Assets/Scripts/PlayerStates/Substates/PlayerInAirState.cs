using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInAirState : PlayerState
{

    private bool coyoteTime;
    private bool isJumping;
    private bool jumpInputStop;

    public PlayerInAirState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void EnterState()
    {
        base.EnterState();
    }

    public override void ExitState()
    {
        base.ExitState();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        jumpInputStop = player.inputHandler.jumpStop;
        CheckCoyoteTime();
        CheckJumpHeight();

        Vector3 velocity = Utils.PhysicsUtils.CorrectVelocityWithSlope(player.inputHandler.movementInput, playerData.speedInAir, player.transform);
        player.controller.Move(velocity * Time.deltaTime);

        player.playerVel.y +=playerData.gravity * Time.deltaTime;
        player.controller.Move(player.playerVel * Time.deltaTime);

        player.anim.SetFloat("ySpeed", player.playerVel.y);
        player.anim.SetFloat("speed",0.6f);

        if(player.grounded && player.playerVel.y < 0.01f)
        {
            stateMachine.ChangeState(player.landState);
        }
        else if(player.inputHandler.jumpInput && player.jumpState.CanJump())
        {
            stateMachine.ChangeState(player.jumpState);
        }
        else if(player.onWall && player.playerVel.y <= 0)
        {
            stateMachine.ChangeState(player.wallSlideState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    private void CheckCoyoteTime()
    {
        if (coyoteTime && Time.time > startTime + playerData.coyoteTime)
        {
            coyoteTime = false;
            player.jumpState.RestAmountOfJumps();
        }
            
    }

    private void CheckJumpHeight()
    {
        if (isJumping)
        {
            if (jumpInputStop)
            {
                player.playerVel = player.playerVel * playerData.jumpHeightMultiplier;
                
                isJumping = false;
            }
            else if (player.playerVel.y <= 0)
                isJumping = false;

            
        }
        if (player.inputHandler.movementInput.x != 0)
            player.playerVel.x = 0;
    }

    public void StartCoyoteTime() => coyoteTime = true;

    public void SetIsJumping() => isJumping = true;
    public bool GetIsJumping() { return isJumping; }
}
