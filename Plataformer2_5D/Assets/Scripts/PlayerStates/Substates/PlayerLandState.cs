using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLandState : PlayerGroundedState
{
    public PlayerLandState(Player player, StateMachine stateMachine, PlayerData playerData, string animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void EnterState()
    {
        base.EnterState();

        player.playerVel.y = 0;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if(Mathf.Abs(player.inputHandler.movementInput.x) > 0.2f && !jumpInput)
        {
            stateMachine.ChangeState(player.moveState);
        }
        else if(isAnimFinish)
        {
            stateMachine.ChangeState(player.idleState);
        }
    }
}
