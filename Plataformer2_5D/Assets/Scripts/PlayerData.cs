using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newPlayerData", menuName = "Scriptables/data")]
public class PlayerData : ScriptableObject
{
    [SerializeField]
    public float speed;

    [Header("Jump")]
    [SerializeField]
    public float speedInAir = 4;
    [SerializeField]
    public float gravity;
    [SerializeField]
    public float coyoteTime = 0.2f;
    [SerializeField]
    public float jumpHeight = 1;
    [SerializeField]
    public float jumpHeightMultiplier = 0.5f;
    [SerializeField]
    public int numOfJumps = 2;

    [Header("Wall")]
    [SerializeField]
    public float wallSlideVel = 1f;
    public float wallJumpForce = 5f;
}
