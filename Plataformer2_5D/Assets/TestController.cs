using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
    private Animator anim;
    private CharacterController controller;
    [SerializeField]
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed = 5.0f;
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0,0);

        if (move.magnitude > 0.1f && groundedPlayer)
            anim.Play("Walk");
        else if (groundedPlayer)
            anim.Play("Idle");

        Vector3 velocity = move * playerSpeed;
        velocity = DescendingSlope(velocity);
        controller.Move(velocity * Time.deltaTime);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            anim.Play("Jump");
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    private Vector3 DescendingSlope(Vector3 velocity)
    {
        var ray = new Ray(transform.position,Vector3.down);

        if(Physics.Raycast(ray,out RaycastHit hitInfo,0.2f))
        {
            var slopeRot = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);

            var adjustVel = slopeRot * velocity;

            if (adjustVel.y < 0)
                return adjustVel;
        }

        return velocity;
    }
}
